import React from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import { renderRoutes } from 'react-router-config'
const App = ({ routes, store }) => (
	<Provider store={store}>
		<Router>
			<Switch> {renderRoutes(routes)} </Switch>
		</Router>
	</Provider>
)

App.propTypes = {
	routes: PropTypes.array.isRequired,
	store: PropTypes.object.isRequired
}
export default App
