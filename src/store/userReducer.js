// ------------------------------------
// Constants
// ------------------------------------

const userstore = function reducer(
	state = {
		userstore: [],
		fetching: false,
		fetched: false,
		error: null,
		isLogin: true
	},
	action
) {
	switch (action.type) {
		case 'FETCH_USERS': {
			return { ...state, fetching: true }
		}
		case 'FETCH_USERS_REJECTED': {
			return { ...state, fetching: false, error: action.payload }
		}
		case 'USER_DELETE_FULFILLED': {
			return {
				...state,
				userstore: action.payload
			}
		}
		case 'NEW_USERS_FULFILLED': {
			return {
				...state,
				fetching: true,
				fetched: true,
				userstore: action.payload
			}
		}
		case 'FETCH_USERS_FULFILLED': {
			return {
				...state,
				fetching: true,
				fetched: true,
				userstore: action.payload
			}
		}
	}
	return state
}
export default userstore
