import { combineReducers } from 'redux'
// import { reducer as notifications } from 'modules/notification'
// import locationReducer from './location'
// import iconstore from './iconStoreReducer'
import userstore from './userReducer'

export const makeRootReducer = asyncReducers => {
	return combineReducers({
		// Add sync reducers here
		userstore,
		...asyncReducers
	})
}

export const injectReducer = (store, { key, reducer }) => {
	store.asyncReducers[key] = reducer
	store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
