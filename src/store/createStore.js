import { applyMiddleware, compose, createStore } from 'redux'
import thunk from 'redux-thunk'
import promise from 'redux-promise'
import logger from 'redux-logger'
// import { browserHistory } from 'react-router'
import createHistory from 'history/createBrowserHistory'
import makeRootReducer from './reducers'
import { version } from '../../package.json'
import { updateLocation } from './location'

export default (initialState = {}) => {
	// ======================================================
	// Window Vars Config
	// ======================================================
	window.version = version

	// ======================================================
	// Middleware Configuration
	// ======================================================
	const middleware = [promise, thunk, logger]

	// ======================================================
	// Store Enhancers
	// ======================================================
	const enhancers = []
	// if (__DEV__) {
	// 	const devToolsExtension = window.devToolsExtension
	// 	if (typeof devToolsExtension === 'function') {
	// 		enhancers.push(devToolsExtension())
	// 	}
	// }

	// Initialize Firebase
	// firebase.initializeApp(fbConfig)
	// Initialize Firestore
	// firebase.firestore()

	// ======================================================
	// Store Instantiation and HMR Setup
	// ======================================================
	const store = createStore(
		makeRootReducer(),
		initialState,
		compose(
			applyMiddleware(...middleware),
			// reactReduxFirebase(firebase, reduxConfig),
			// reduxFirestore(firebase),
			...enhancers
		)
	)
	store.asyncReducers = {}

	// To unsubscribe, invoke `store.unsubscribeHistory()` anytime
	const history = createHistory()
	store.unsubscribeHistory = history.listen(updateLocation(store))

	if (module.hot) {
		module.hot.accept('./reducers', () => {
			const reducers = require('./reducers').default
			store.replaceReducer(reducers(store.asyncReducers))
		})
	}

	return store
}
