import React from 'react'
import ReactDOM from 'react-dom'
import createStore from './store/createStore'
import './index.css'
import App from './App'
import routes from './routes/index'
import registerServiceWorker from './registerServiceWorker'
import { version } from '../package.json'

window.version = version

// Store Initialization
// ------------------------------------
const initialState =
	window.___INITIAL_STATE__ ||
	{
		// firebase: { authError: null }
	}
const store = createStore(initialState)

ReactDOM.render(<App store={store} routes={routes} />, document.getElementById('root'))
registerServiceWorker()
