import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
// import classes from './Navbar.scss'

export const Navbar = ({
	avatarUrl,
	displayName,
	authExists,
	goToAccount,
	handleLogout,
	closeAccountMenu,
	anchorEl,
	handleMenu
}) => (
	<section className="hero__area">
		<header>
			<div className="container-fluid">
				<div className="row align-items-center">
					<div className="order-1  order-md-1 col-6 col-md-auto">
						<Link className="icofont__logo" to={'/'}>
							Logo
						</Link>
					</div>
					<div className="order-3 order-md-2 col">
						<nav className="icofont__menu d-none d-md-block">
							<ul>
								<li>
									<Link to="/"> Home </Link>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>
	</section>
)

Navbar.propTypes = {
	displayName: PropTypes.string, // from enhancer (flattenProps - profile)
	avatarUrl: PropTypes.string, // from enhancer (flattenProps - profile)
	authExists: PropTypes.bool, // from enhancer (withProps - auth)
	goToAccount: PropTypes.func.isRequired, // from enhancer (withHandlers - router)
	handleLogout: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
	closeAccountMenu: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
	handleMenu: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
	anchorEl: PropTypes.object // from enhancer (withStateHandlers - handleMenu)
}

export default Navbar
