import React from 'react'
import PropTypes from 'prop-types'
// import classes from './Footer.scss'
import { Link } from 'react-router-dom'

export const Footer = ({ displayName }) => (
	<footer>
		<div className="container small__container">
			<div className="row align-items-center">
				<div className="col-12 col-lg-6">
					<div className="icofont__copyright">©2018 Custom. All Rights Reserved.</div>
				</div>
				<div className="col-12 col-lg-6">
					<div className="icofont__footer__menu">
						<Link to="#">Blog</Link>
						<Link to="#">Discussion</Link>
					</div>
				</div>
			</div>
		</div>
	</footer>
)

Footer.propTypes = {
	displayName: PropTypes.string
}

export default Footer
