import CoreLayout from '../layouts/CoreLayout'
import Home from './Home'
import About from './About'
import NotFoundRoute from './NotFound'

const routes = [
	{
		component: CoreLayout,
		routes: [
			{
				path: '/',
				exact: true,
				component: Home
			},
			{
				path: '/about',
				exact: true,
				component: About
			},
			{
				path: '*',
				component: NotFoundRoute
			}
		]
	}
]

export default routes
