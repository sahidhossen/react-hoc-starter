import { compose } from 'redux'
import { withHandlers, pure, withState, lifecycle } from 'recompose'
export default compose(
	withState('state', 'setState', {}),
	withHandlers({
		// customFunc: props => args => {}
	}),
	lifecycle({
		componentDidMount() {
			console.log('About props: ', this.props) // eslint-disable-line no-console
		}
	}),
	pure
)
