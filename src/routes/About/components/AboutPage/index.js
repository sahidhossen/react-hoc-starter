import AboutPage from './AboutPage'
import enhance from './About.enhancer'

export default enhance(AboutPage)
