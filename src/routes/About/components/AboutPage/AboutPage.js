import React from 'react'
import PropTypes from 'prop-types'
import { Link, BrowserRouter } from 'react-router-dom'
export const About = ({ history }) => (
	<div className="flex-column-center">
		<h1> About Us Page </h1>
		<Link to={'/'}> Home Back </Link>
		<a href="#" onClick={e => BrowserRouter.goBack}>
			Back
		</a>
	</div>
)
About.propTypes = {
	history: PropTypes.object
}
export default About
