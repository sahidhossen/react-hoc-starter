import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'

class Drag extends Component {
	constructor(props) {
		super(props)
		this.state = {
			active: false,
			pos: { x: 0, y: 0 },
			height: 40
		}
	}

	componentDidMount() {
		window.addEventListener('mousemove', this.onMouseMoveAction.bind(this))
		window.addEventListener('mouseup', this.onDragStopAction.bind(this))
	}

	componentWillUnmount() {
		window.removeEventListener('mousemove', this.onMouseMoveAction.bind(this))
		window.addEventListener('mouseup', this.onDragStopAction.bind(this))
	}
	onDragStopAction(event) {
		this.setState({ active: false })
	}
	onMouseMoveAction(event) {
		console.log(this.state.active)
		if (this.state.active) {
			let height = this.state.height + (this.state.pos.y - event.pageY)
			this.setState({ height })
			this.setState({ pos: { x: event.pageX, y: event.pageY } })
		}
	}
	onMouseDownAction(event) {
		this.setState({ active: true, pos: { x: event.pageX, y: event.pageY } })
	}

	onMouseLeaveAction(event) {
		// this.setState({ active: false })
	}

	render() {
		return (
			<div className="App">
				<header className="App-header">
					<img src={logo} className="App-logo" alt="logo" />
					<h1 className="App-title">Welcome to React</h1>
				</header>
				<div
					onMouseLeave={this.onMouseLeaveAction.bind(this)}
					onMouseDown={this.onMouseDownAction.bind(this)}
					style={{
						height: this.state.height,
						position: 'absolute',
						width: 200,
						background: 'red'
					}}>
					Update
				</div>
				<p className="App-intro">
					To get started, edit <code>src/App.js</code> and save to reload.
				</p>
			</div>
		)
	}
}

export default App
