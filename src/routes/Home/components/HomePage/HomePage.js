import React from 'react'
// import { Link } from 'react-router'
// import { ICON_LIST_PATH, ICON_GENERATOR_PATH } from 'constants'
import { Link } from 'react-router-dom'
export const Home = () => (
	<div className="homePage">
		<h1> Home Page </h1>
		<Link to={'/about'}> About Page </Link>
	</div>
)

export default Home
