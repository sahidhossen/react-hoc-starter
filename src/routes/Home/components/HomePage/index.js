import HomePage from './HomePage'
import enhance from './Home.enhancer'

export default enhance(HomePage)
