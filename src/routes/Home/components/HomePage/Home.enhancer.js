import { compose } from 'redux'
import { connect } from 'react-redux'
// import { withRouter } from 'utils/components'
// import { withNotifications } from 'modules/notification'
import { withHandlers, pure, withState, lifecycle } from 'recompose'
import { userIsAuthenticated } from 'utils/services'

export default compose(
	connect(store => {
		return { userstore: store.userstore }
	}),
	userIsAuthenticated,
	// Show loading spinner while icons and collabIcons are loading
	// spinnerWhileLoading(['icons']),
	// Add props.router
	// withRouter,
	// Add props.showError and props.showSuccess
	withState('state', 'setState', {}),
	withHandlers({
		// customFunc: props => args => {}
	}),
	lifecycle({
		componentDidMount() {
			console.log('home props: ', this.props) // eslint-disable-line no-console
		}
	}),
	pure
)
