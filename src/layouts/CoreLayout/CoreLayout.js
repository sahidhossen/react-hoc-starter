import React from 'react'
import PropTypes from 'prop-types'
import Navbar from '../../elements/Navbar'
import Footer from '../../elements/Footer'
// import { Notifications } from 'modules/notification'
// import 'styles/core.scss'
import { renderRoutes } from 'react-router-config'

export const CoreLayout = ({ route }) => (
	<div className={'site__content body-'}>
		<Navbar />
		{renderRoutes(route.routes)}
		<Footer />
		{/* <Notifications /> */}
	</div>
)

CoreLayout.propTypes = {
	route: PropTypes.shape().isRequired
}

export default CoreLayout
