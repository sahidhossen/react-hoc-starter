export const ICON_GENERATOR_PATH = '/generators'
export const LICENSE_PATH = '/license'
export const ACCOUNT_PATH = '/account'
export const LOGIN_PATH = '/login'
export const SIGNUP_PATH = '/signup'
export const ACCOUNT_FORM_NAME = 'account'
export const LOGIN_FORM_NAME = 'login'
export const SIGNUP_FORM_NAME = 'signup'
export const NEW_PROJECT_FORM_NAME = 'newProject'
export const NEW_ICON_FORM_NAME = 'newIcon'

export const formNames = {
	account: ACCOUNT_FORM_NAME,
	signup: SIGNUP_FORM_NAME,
	login: LOGIN_FORM_NAME
}

export const paths = {
	account: ACCOUNT_PATH,
	login: LOGIN_PATH,
	signup: SIGNUP_PATH
}

export default { ...paths, ...formNames }
